import java.util.Scanner;

class User
{
    void in() {
        System.out.println("Enter your name : ");
        Scanner input = new Scanner(System.in);
        String name = input.nextLine();
        System.out.println("Hello " + name);
    }
}

class HelloUserJava
{
    public static void main(String[] args)
    {
        User obj = new User();
        obj.in();
    }
}
