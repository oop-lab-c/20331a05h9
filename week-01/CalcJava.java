import java.util.Scanner;

class CalcJava {
    public static void main(String[] args) {
        System.out.println("Enter two numbers : ");
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        System.out.println("Enter any option(+,-,*,/,%):");
        char c = input.next().charAt(0);
        if (c == '+') {
            System.out.println(a + b);
        } else if (c == '-') {
            System.out.println(a - b);
        } else if (c == '*') {
            System.out.println(a * b);
        } else if (c == '%') {
            if (b == 0) {
                System.out.println("Modulus operation is not possible...Zero error!!\n");
            } else {
                System.out.println(a % b);
            }
        } else if (c == '/') {
            if (b == 0) {
                System.out.println("Division operation is not possible...Zero error!!\n");
            } else {
                System.out.println(a / b);
            }
        } else {
            System.out.println("Enter valid option");
        }
    }
}
