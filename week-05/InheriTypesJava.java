class bird {
    void fly() {
        System.out.println("I can fly");
    }
}

class parrot extends bird {
    void eat() {
        System.out.println("I eat guava");
    }
}

class childparrot extends parrot // multiple inheitance
{
    public static void main(String[] args) {
        childparrot p = new childparrot();
        p.fly();
        p.eat();
    }
}

class crow extends bird // simple inheritance
{
    void colour() {
        System.out.println("i am in black colour");
    }

    public static void main(String[] args) {
        crow c = new crow();
        c.fly();
        c.colour();
    }
}
