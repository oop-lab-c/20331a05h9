#include<iostream>
using namespace std;
class bird
{
    public:
    void fly()
    {
        cout<<"i fly in the sky"<<endl;
    }
};
class parrot:public bird
{
    public:
    void eat()
    {
        cout<<"i eat guava"<<endl;
    }
};
int main()
{
    parrot obj;
    obj.fly();
    obj.eat();
    return 0;
}